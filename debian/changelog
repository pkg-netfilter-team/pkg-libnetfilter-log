libnetfilter-log (1.0.2-4) unstable; urgency=medium

  * [17d197f] d/control: use tracker.d.o address for `Maintainer:`
  * [19c7b35] d/control: bump Standards-Version to 4.6.2
  * [1c8c620] d/control: use `Build-Depends-Indep` for -doc package
  * [897f16e] d/control, d/rules: add `nodoc` build-profile support
  * [20decb2] d/gbp.conf: set `pristine-tar` once in `DEFAULT`
  * [447314e] d/gbp.conf: dch: update `commit-msg`

 -- Jeremy Sowden <jeremy@azazel.net>  Wed, 09 Aug 2023 21:58:04 +0100

libnetfilter-log (1.0.2-3) unstable; urgency=medium

  * [7a11446] d/rules: remove export of dpkg build-flags
  * [1a9ca7a] d/control: add build-dep on graphviz
  * [d3b35c5] d/control: restore dependencies removed from -dev package
    (closes: #1009403)
  * [d0c23ce] d/control: remove obsolete version constraints from -dev deps

 -- Jeremy Sowden <jeremy@azazel.net>  Wed, 11 May 2022 15:16:14 +0100

libnetfilter-log (1.0.2-2) unstable; urgency=medium

  * [ad14212] d/gbp.conf: add dch commit-msg
  * [770c36b] d/: add doc-base file for -doc package
  * [fd3d16d] d/control: remove superfluous dependencies from -dev package
  * [1d32955] d/control: remove obsolete version constraints from b-deps
  * [09b6002] d/control: use `linux-any` for arch-specific packages
  * [c65ae01] d/control: add `${misc:Pre-Depends}` to -dev
  * [5aa4f1d] d/control: add `Multi-Arch: forgeign` to -doc package
  * [7effe5e] d/copyright: update copyright fields
  * [1f2d611] d/gbp.conf: sort sections
  * [3b171fa] d/libnetfilter-log1.symbols: add `Build-Depends-Package` fields
  * [85752ca] d/patches: add patch with doxygen correction
  * [9bc9020] d/rules: enable hardening
  * [a66e2f4] d/rules: export dpkg build-flags
  * [791cdd1] d/upstream/signing-key.asc: replace with minimally exported key
  * [55d9a6d] d/watch: use HTTPS URL

 -- Jeremy Sowden <jeremy@azazel.net>  Sun, 10 Apr 2022 13:28:19 +0100

libnetfilter-log (1.0.2-1) unstable; urgency=medium

  * [7be211f] d/libnetfilter-log-doc.examples: add test sources to -doc
    (closes: #609014).
  * [cb2e1d6] New upstream version 1.0.2
  * [c3211b4] d/patches: remove upstreamed patch
  * [e9ca78a] d/control: bump Standards-Version to 4.6.0
  * [1a91ab2] d/control: add new build-dep on libmnl
  * [e645662] d/control: bump -dev dependency on libnfnetlink-dev to match
    version of build-dep
  * [abace68] d/control: add -dev dependency on libmnl-dev
  * [bde2612] d/libnetfilter-log1.symbols: update symbols
  * [ef5fc34] Upstream now installs HTML doc's.
  * [d95daa4] Include man-pages in -doc package.
  * [8fe6dc1] d/watch: bump version to 4.
  * [9362573] d/watch: enable signature verification.
  * [65205a9] d/gbp.conf: enable pristine-tar for buildpackage

 -- Jeremy Sowden <jeremy@azazel.net>  Fri, 19 Nov 2021 22:09:55 +0000

libnetfilter-log (1.0.1-3) unstable; urgency=medium

  * Team upload.
  * New source-only upload to unstable to allow testing migration.

 -- Arturo Borrero Gonzalez <arturo@debian.org>  Thu, 28 Jan 2021 16:45:26 +0100

libnetfilter-log (1.0.1-2) unstable; urgency=medium

  * [67cd799] d/gbp.conf:
    + set dch --id-length to 7;
    + set import-orig --pristine-tar to true.
  * [0fe3b20] d/: remove compat and add a build-dep on `debhelper-compat = 13`
    to control.
  * [aebc7d4] d/: remove -dbg package.
  * [9bea331] d/: remove explicit invocation of dh_autoreconf.
  * [ea7cbf4] d/: run wrap-and-sort.
  * [2c7446a] d/control: move -log1 Depends and Pre-Depends.
  * [b275c54] d/control: remove ${shlib:Depends} from -dev Depends.
  * [e1c2bac] d/control: bump Standards-Version to 4.5.0.
  * [234cb1d] d/control: change priority to optional.
  * [ebf4a1b] d/control: change maintainer to the NF packaging team.
  * [e1abb54] d/control: add myself to uploaders.
  * [0827ad8] d/control: add VCS URL's.
  * [8c1718b] d/control: add home-page.
  * [52321ac] d/control: add build-deps on libtool and pkg-config.
  * [e7d5dcf] d/control: add Rules-requires-root: no.
  * [10092b3] d/not-installed: add .la files.
  * [a8e3ca1] d/libnetfilter-log1.symbols: add symbols file.
  * [6d54e9e] d/.gitignore: add a .gitignore file.
  * [c1f6f0e] d/copyright: update to DEP-5 format.
  * [27178a3] d/changelog: remove trailing white-space.
  * [6c659b3] d/patches: add a patch to fix missing linkage of
    libnetfilter_log_libipulog.so to libnfnetlink.so.
  * [97214b9] d/libnetfilter-log-dev.dirs: the directory in the file is
    created automatically, so remove the file.
  * [fd05201] d/: add a -doc package.
    + Add an override to run doxygen after dh_auto_build and generate the API
      documentation.
    + Add the HTML API doc's to the -doc package.
  * [8d15ee8] d/control: bump Standards-Version to 4.5.1.
  * [2a657c3] d/gbp.conf: change debian branch-name to "debian"
  * [8c1b6b2] d/control: remove ${misc:Pre-Depends} from -dev Depends.

 -- Jeremy Sowden <jeremy@azazel.net>  Sat, 05 Dec 2020 13:09:07 +0000

libnetfilter-log (1.0.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix libnetfilter-log-dev and libnetfilter-log1-dbg symlink to dir.
    (Closes: #768287)
    - New libnetfilter-log1.maintscript and libnetfilter-log.maintscript files.
    - Add ${misc:Pre-Depends}.

 -- Jean-Michel Nirgal Vourgère <jmv_deb@nirgal.com>  Sun, 16 Nov 2014 11:03:35 +0000

libnetfilter-log (1.0.1-1) unstable; urgency=medium

  * [4f4ea66] Imported Upstream version 1.0.1
  * [17d02c0] Bump standards version
  * [045f84b] Move to dh9 and multiarch

 -- Alexander Wirt <formorer@debian.org>  Sun, 26 Oct 2014 00:56:37 +0200

libnetfilter-log (1.0.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use dh-autoreconf in build to support new architectures (Closes: 727418)

 -- Chen Baozi <baozich@gmail.com>  Thu, 05 Jun 2014 20:21:34 +0800

libnetfilter-log (1.0.0-1) unstable; urgency=low

  * New upstream version (Closes: #589157)
  * Update maintainer address
  * Bump standards version
  * Move libnetfilter-log1-dbg to section debug
  * Add ${misc:Depends} to binary packages

 -- Alexander Wirt <formorer@debian.org>  Thu, 16 Dec 2010 15:13:31 +0100

libnetfilter-log (0.0.16-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release
  * build with libnfnetlink-dev 0.0.41

  [ Alexander Wirt ]
  * Bump standards version (no changes)
  * Fix copyright file

 -- Alexander Wirt <formorer@debian.org>  Thu, 02 Apr 2009 11:05:59 +0200

libnetfilter-log (0.0.15-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release (Closes: #494947)
    - increased shlibs version to 0.0.15 because of API additions
  * fixed duplicated word in description
  * bumped Standards-Version to 3.8.0
  * moved DH_COMPAT to debian/compat
  * don't ignore "make distclean" errors
  * use ${binary:Version} instead of ${Source-Version}

  [ Alexander Wirt ]
  * Use http in watchfile

 -- Alexander Wirt <formorer@debian.org>  Fri, 17 Oct 2008 13:12:33 +0200

libnetfilter-log (0.0.13-1) unstable; urgency=low

  * initial debian release (Closes: #388730)

 -- Max Kellermann <max@duempel.org>  Fri, 22 Sep 2006 13:43:26 +0200
