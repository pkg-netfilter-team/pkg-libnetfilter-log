Source: libnetfilter-log
Section: libs
Priority: optional
Maintainer: Debian Netfilter Packaging Team <team+pkg-netfilter-team@tracker.debian.org>
Uploaders: Alexander Wirt <formorer@debian.org>,
           Jeremy Sowden <jeremy@azazel.net>
Build-Depends: debhelper-compat (= 13),
               libmnl-dev,
               libnfnetlink-dev,
               libtool,
               pkg-config
Build-Depends-Indep: doxygen <!nodoc>, graphviz <!nodoc>
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.netfilter.org/projects/libnetfilter_log
Vcs-Git: https://salsa.debian.org/pkg-netfilter-team/pkg-libnetfilter-log.git
Vcs-Browser: https://salsa.debian.org/pkg-netfilter-team/pkg-libnetfilter-log

Package: libnetfilter-log1
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Netfilter netlink-log library
 libnetfilter_log is a userspace library providing interface to
 packets that have been logged by the kernel packet filter. It is part
 of a system that deprecates the old syslog/dmesg based packet
 logging.

Package: libnetfilter-log-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libmnl-dev,
         libnetfilter-log1 (= ${binary:Version}),
         libnfnetlink-dev,
         pkg-config,
         ${misc:Depends}
Suggests: libnetfilter-log-doc (= ${binary:Version})
Description: Development files for libnetfilter-log1
 libnetfilter_log is a userspace library providing interface to
 packets that have been logged by the kernel packet filter. It is part
 of a system that deprecates the old syslog/dmesg based packet
 logging.
 .
 This package provides development files and static libraries.

Package: libnetfilter-log-doc
Section: doc
Architecture: all
Build-Profiles: <!nodoc>
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}
Description: Documentation files for libnetfilter-log1
 libnetfilter_log is a userspace library providing interface to
 packets that have been logged by the kernel packet filter. It is part
 of a system that deprecates the old syslog/dmesg based packet
 logging.
 .
 This package provides HTML documentation and man-pages for the API.
